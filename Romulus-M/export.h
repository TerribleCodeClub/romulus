/**
 * BUTLERSAURUS 2022 addition
 *
 * This header defines a cross-platform EXPORT macro in order to allow us to
 * build this reference implementation as a library.
 */
#pragma once

#ifdef _WIN32
#define EXPORT __declspec(dllexport)
#elif __linux__
#define EXPORT __attribute__((visibility("default")))
#elif __EMSCRIPTEN__
#include <emscripten.h>
#define EXPORT EMSCRIPTEN_KEEPALIVE
#endif
