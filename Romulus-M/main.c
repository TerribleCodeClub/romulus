/**
 * BUTLERSAURUS 2022 addition
 *
 * This file exists to create a fake dummy executable target, which is required
 * when compiling for WASM.
 */
int main(void)
{
    return 0;
}
